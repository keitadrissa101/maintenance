<?php 
   include('security.php'); 

   include('includes/header.php'); 
   include('includes/navbar.php');  
?> 

 <!-- Content Wrapper -->
 <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Topbar -->
  <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
      <div class="input-group">
        <input type="text" class="form-control bg-light border-0 small" placeholder="Rechercher..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search fa-sm"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

      <!-- Nav Item - Search Dropdown (Visible Only XS) -->
      <li class="nav-item dropdown no-arrow d-sm-none">
        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-search fa-fw"></i>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
          <form class="form-inline mr-auto w-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Nav Item - Alerts -->
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <!-- Counter - Alerts -->
          <span class="badge badge-danger badge-counter">3+</span>
        </a>
        <!-- Dropdown - Alerts -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
          <h6 class="dropdown-header">
            Alerts Center
          </h6>
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="mr-3">
              <div class="icon-circle bg-primary">
                <i class="fas fa-file-alt text-white"></i>
              </div>
            </div>
            <div>
              <div class="small text-gray-500">December 12, 2019</div>
              <span class="font-weight-bold">A new monthly report is ready to download!</span>
            </div>
          </a>
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="mr-3">
              <div class="icon-circle bg-success">
                <i class="fas fa-donate text-white"></i>
              </div>
            </div>
            <div>
              <div class="small text-gray-500">December 7, 2019</div>
              $290.29 has been deposited into your account!
            </div>
          </a>
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="mr-3">
              <div class="icon-circle bg-warning">
                <i class="fas fa-exclamation-triangle text-white"></i>
              </div>
            </div>
            <div>
              <div class="small text-gray-500">December 2, 2019</div>
              Spending Alert: We've noticed unusually high spending for your account.
            </div>
          </a>
          <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
        </div>
      </li>

      <!-- Nav Item - Messages -->
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <!-- Counter - Messages -->
          <span class="badge badge-danger badge-counter">7</span>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
          <h6 class="dropdown-header">
            Message Center
          </h6>
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
              <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
              <div class="status-indicator bg-success"></div>
            </div>
            <div class="font-weight-bold">
              <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
              <div class="small text-gray-500">Emily Fowler · 58m</div>
            </div>
          </a>
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
              <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
              <div class="status-indicator"></div>
            </div>
            <div>
              <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
              <div class="small text-gray-500">Jae Chun · 1d</div>
            </div>
          </a>
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
              <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
              <div class="status-indicator bg-warning"></div>
            </div>
            <div>
              <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
              <div class="small text-gray-500">Morgan Alvarez · 2d</div>
            </div>
          </a>
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
              <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
              <div class="status-indicator bg-success"></div>
            </div>
            <div>
              <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
              <div class="small text-gray-500">Chicken the Dog · 2w</div>
            </div>
          </a>
          <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
        </div>
      </li>

      <div class="topbar-divider d-none d-sm-block"></div>

      <!-- Nav Item - User Information -->
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="mr-2 d-none d-lg-inline text-gray-600 small">
            <?php echo $_SESSION['username']; ?>  
          </span>
          <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="#">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            Profil
          </a>
          <a class="dropdown-item" href="register.php">
            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
            Paramètres
          </a>
          <a class="dropdown-item" name="pass_charger" href="change_password.php">
            <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
            Changer mot de passe
          </a>
          <a class="dropdown-item" href="envoi.php">
            <i class="fas fa-envelope fa-sm fa-fw mr-2 text-gray-400"></i>
            Envoyer SMS
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="index.php" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
            Déconnexion
          </a>
        </div>
      </li>

    </ul>

  </nav>
  <!-- End of Topbar -->
  <div class="container-fluid"> 
  <!-- DataTables Example--->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="n-0 font-weight-bold text-primary">Edition de Contacts
        <i class="fas fa-address-book fa-sm fa-fw mr-2 text-gray-800"></i>
        </h6>
    </div>
    <div class="card-body">
    
    <?php
        $connection = mysqli_connect("localhost","root","Habibatou15","thorodosms");
        
        if(isset($_POST['edit_contacts_btn']))
        {
            $id = $_POST['edit_id'];
            $query = "SELECT * FROM contacts WHERE id='$id' ";
            $query_run = mysqli_query($connection, $query);

            foreach($query_run as $row)
        {
    ?>

<form action="code.php" method="POST">

<input type="hidden" name="edit_id" value="<?php echo $row['id'] ?>" >
  <div class="form-row">
    <div class="form-group col-md-6">
      <label >Nom</label>
      <input type="text" name="edit_name" value="<?php echo $row['name'] ?>" class="form-control" >
    </div>
    <div class="form-group col-md-6">
      <label>Téléphone Mobile</label>
      <input type="text" name="edit_mobile" value="<?php echo $row['mobile'] ?>" class="form-control" placeholder="Prénom">
    </div>
  </div>
  <div class="form-row">
      <div class="form-group col-md-6">
      <label>Téléphone Fix</label>
      <input type="text" name="edit_telephone" value="<?php echo $row['telephone'] ?>" class="form-control" placeholder="Email">
    </div>
  <div class="form-group col-md-6">
    <label>Fax</label>
    <input type="text" name="edit_fax" value="<?php echo $row['fax'] ?>" class="form-control" placeholder="Date de naissance">
  </div>
  </div>
  <div class="form-row">
      <div class="form-group col-md-6">
      <label>Email</label>
      <input type="text" name="edit_email" value="<?php echo $row['email'] ?>" class="form-control" placeholder="Email">
    </div>
  <div class="form-group col-md-6">
    <label>Adresse</label>
    <input type="text" name="edit_im" value="<?php echo $row['im'] ?>" class="form-control" placeholder="Date de naissance">
  </div>
  </div>
 <div class="form-row">
    <div class="form-group col-md-4">
      <label>Autre Adresse</label>
      <input name="edit_other" type="text" value="<?php echo $row['other'] ?>" class="form-control" placeholder="Numéro de téléphone...">
    </div>
    <div class="form-group col-md-4">
      <label>Description</label>
      <input type="text" name="edit_comment" class="form-control" value="<?php echo $row['comment'] ?>" placeholder="commentaire....">
    </div>
      <div class="form-group col-md-4">
        <label>Date</label>
        <input name="edit_date" value="<?php echo $row['createTime'] ?>" class="form-control">
    </div>
 </div>
 <div class="form-row">
          <div class="form-group col-md-6">
                <label>Nom Groupe</label>
                <input type="text" name="edit_nom_group" value="<?php echo $row['nom_group'] ?>" class="form-control" placeholder="Entrer le nom du groupe ou de l'école">
            </div>
              <div class="form-group col-md-6">
                  <label>Groupes Types</label>
                  <select name="update_type_groupe" class="form-control">
                  <option value="<?php echo $row['type_groupe'] ?>"><?php echo $row['type_groupe'] ?><option>
                  <option value="Admin">Admin</option>
                  <option value="Etudiants">Etudiants</option>
                  <option value="Professeurs">Professeurs</option>
                  <option value="Parents">Parents</option>
              </select>
              </div>
            </div>
            <div class="form-row">
            <div class="form-group col-md-6">
                <label>Filière</label>
                <select name="update_filiere"  class="form-control">
                <option value="<?php echo $row['filiere'] ?>"><?php echo $row['filiere'] ?><option>
                <option value="TELECOM">TELECOM</option>
                <option value="COMEN">COMEN</option>
                <option value="INFOGES">INFOGES</option>
                <option value="FINANCECOMPTA">FINANCECOMPTA</option>
                <option value="MANANGEMENT">MANAGEMENT</option>
                <option value="MARKETING & COMMERCE">MARKETING & COMMERCE</option>
                <option value="DROITS">DROITS</option>
                <option value="SCIENCES POLITIQUES">SCIENCES POLITIQUES</option>
                <option value="MEDECINE">MEDECINE</option>
                <option value="ELECTRONIQUE & INDUSTRIES">ELECTRONIQUE & INDUSTRIES</option>
                <option value="SANTE & HYGIENE">SANTE & HYGIENE</option>
                <option value="GENIE CIVILE">GENIE CIVILE</option>
                <option value="ARCHITECTURE">ARCHITECTURE</option>
                                
            </select>
            </div>
            <div class="form-group col-md-6">
                <label>Classe</label>
                <select name="update_classe" class="form-control">
                <option value="<?php echo $row['classe'] ?>"><?php echo $row['classe'] ?><option>
                <option value="LICENCE 1" >LICENCE 1</option>
                <option value="LICENCE 2">LICENCE 2</option>
                <option value="LICENCE 3">LICENCE 3</option>
                <option value="MASTER 1">MASTER 1</option>
                <option value="MASTER 2">MASTER 2</option>
                <option value="DEUG">DEUG</option>
                <option value="DOCTORAT">DOCTORAT</option>
                               
            </select>
            </div>
          </div>

    <a href="contacts_list.php" class="btn btn-danger"> CANCEL </a>
    <button type="submit" name="update_contacts_btn" class="btn btn-primary"> Update </button>

</form>
<?php
    }
}
?>
    </div>
    </div>
</div>


</div>
<!-- DataTables Example -->


</div>
</div>


<?php 
   include('includes/footer.php');
   include('includes/scripts.php'); 
?>