<?php 
include('config/database.php');
include('includes/header.php');
include('includes/navbar.php');
 ?>
<?php
if(isset($_POST['valider'])){
	if (isset( $_GET['id'])) {
		
		if (!empty($_POST['code_contrat']) && !empty($_POST['libelle']) && !empty($_POST['date_debut']) && !empty($_POST['date_fin']) && !empty($_POST['montant']) && !empty($_POST['type_contrat']) && !empty($_POST['frequence'])) {
			
			$errors=[];//tableaux contenant l'ensemble des erreurs
			extract($_POST);// extaction pour y avoir access aux donnes des champs
				$q = $db->prepare('INSERT INTO contrat(idclt, code_contrat, libelle, date_debut, date_fin,  montant, type_contrat, frequence) VALUES(:idclt, :code_contrat,  :libelle, :date_debut, :date_fin,  :montant, :type_contrat, :frequence)');

      		$q->execute([
      			       'idclt'=> $_GET['id'],
      			        'code_contrat' => $code_contrat,
                        'libelle' => $libelle,
                        'date_debut' => $date_debut,
                        'date_fin' => $date_fin,
                        'montant' => $montant,                      
                        'type_contrat' => $type_contrat,
                        'frequence' => $frequence
      		]);
			}else {
				echo "veillez remplir tous les champs";
			}
		}
	}

	include('views/ajoutecontrat.views.php');


include('includes/footer.php');
include('includes/scripts.php');
?>