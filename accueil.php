<?php
include('config/database.php');
include('includes/header.php');
include('includes/navbar.php');

?>


 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h2 class="m-0 text-dark">Tableau de Bord</h2>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
              <li class="breadcrumb-item active">Tableau de Bord</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>
                    <?php 
                   //require_once 'system/dbconfig.php';
                    $b= $db->query("SELECT id_admin FROM admin ORDER BY id_admin");
                    ?> 
                    <?php while ($d= $b->fetch()) {?>
                    echo "$d";
                    <?php } ?>

                </h3>

                <p>Nombre Utilisateur</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            <a href="../listes/listutilisateur.php" class="small-box-footer">Plus d'infos <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>
                    <?php

                          //require_once 'system/dbconfig.php';
                       $b= $db->query("SELECT idclt FROM client ORDER BY idclt");  
                    ?>
                    <?php while ($d= $b->fetch()) {?>
                    echo "$d";
                    <?php } ?>

                </h3>

                <p>Nombre Client</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
           
              <a href="../listes/listeclient.php" class="small-box-footer">Plus d'infos <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3> <?php 
                   //require_once 'system/dbconfig.php';
                    $b= $db->query("SELECT id_contrat FROM contrat ORDER BY id_contrat");
                    ?> </h3>
                  <?php while ($d= $b->fetch()) {?>
                    echo "$d";
                    <?php } ?>

                <p>Nombre de Contrat</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="../listes/listecontrat.php" class="small-box-footer">Plus d'infos <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3> <?php 
                   //require_once 'system/dbconfig.php';
                    $b= $db->query("SELECT id_eq FROM equipement ORDER BY id_eq");
                    ?>
                   <?php while ($d= $b->fetch()) {?>
                    echo "$d";
                    <?php } ?>
                    
                     </h3>

                <p>Nombre d'équipement</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="../listes/listequipement.php" class="small-box-footer">Plus d'infos <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Sales
                </h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active"  data-toggle="tab">Area</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link"  data-toggle="tab">Donut</a>
                    </li>
                  </ul>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <!-- Google chart - Sales -->
                  <div class="chart tab-pane active" style="position: relative; height: 300px;">
                      <div id="line_chart"></div>                         
                   </div>
                  <div class="chart tab-pane" style="position: relative; height: 300px;">
                    <div id="piechart_div"></div>                        
                  </div>  
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
            
          </section>
          <!-- /.Left col -->
         <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

             <!-- Map card -->
            <div class="card bg-gradient-primary">
              <div class="card-header border-0">
                <h3 class="card-title">
                  <i class="fas fa-map-marker-alt mr-1"></i>
                  Visitors
                </h3>
                <!-- card tools -->
                <div class="card-tools">
                  <button type="button"
                          class="btn btn-primary btn-sm daterange"
                          data-toggle="tooltip"
                          title="Date range">
                    <i class="far fa-calendar-alt"></i>
                  </button>
                  <button type="button"
                          class="btn btn-primary btn-sm"
                          data-card-widget="collapse"
                          data-toggle="tooltip"
                          title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <div class="card-body">
                <div id="world-map" style="height: 250px; width: 100%;"></div>
              </div>
              <!-- /.card-body-->
              <div class="card-footer bg-transparent">
                <div class="row">
                  <div class="col-4 text-center">
                    <div id="sparkline-1"></div>
                    <div class="text-white">Visitors</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-2"></div>
                    <div class="text-white">Online</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-3"></div>
                    <div class="text-white">Sales</div>
                  </div>
                  <!-- ./col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->





<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript"> 
  google.charts.load('current', {'packages':['corechart']});

  google.charts.setOnLoadCallback(line_chart);
  google.charts.setOnLoadCallback(pie_chart);


  function pie_chart(){
    var jsonData = $.ajax({
      url : 'chart/pie_chart.php',
      dataType : 'json',
      async : false
    
    }).responseText;
    var data = new google.visualization.DataTable(jsonData);
    var chart = new google.visualization.PieChart(document.getElementById('piechart_div'));
    chart.draw(data);
  }


  function line_chart(){
    var jsonData = $.ajax({
      url : 'chart/line_chart.php',
      dataType : "json",
      async : false,
      success : funtion(jsonData)
      {
        var options = 
        {
          legend : 'none',
          hAxis:{ minValue : 0, maxValue: 9},
          curveType: 'function',
          pointSize: 7,
          dataOpacity: 0.3

        };
        var data = new google.visualization.arrayToDataTable(jsonData);
        var chart = new google.visualization.LineChart(document.getElementById('line_chart'));
        chart.draw(data,options);

      }
    }).responseText;
  }

</script>



<?php
include('../includes/footer.php');
include('../includes/scripts.php');

?>