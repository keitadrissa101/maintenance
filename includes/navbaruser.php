  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
    <img src="../img/logo.png" width="80px" class="img-circle elevation-2" href="index.php"><br>
      <span >
                <?php 
          
          require_once '../system/dbconfig.php';
          $username = $_SESSION['username'];
          $query = "SELECT *FROM users WHERE username='$username' ";
          $query_run = mysqli_query($connection,$query);
          echo "$username";
          ?>  
         </span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <form action="../partiel/listutilisateur.php" method="post"> 
          <button class="btn btn- " name="editbtn" type="submit"><img src="../img/login.png" class="img-circle elevation-4" alt="User Image"></button>
      </form>
        </div>
        <div class="info">
          <a href="#" class="d-block">
    
          <?php 
          
          require_once '../config/database.php';
          $username = $_SESSION['username'];
          $query = "SELECT *FROM users WHERE username='$username' ";
          $query_run = mysqli_query($connection,$query);
          echo "$username";
          
          ?>
          
          
          </a>
          <a href="#" class="small-box-footer"> <i class="fas fa-admin"></i></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                         <!---- MENU ACCUEIL------>
          <li class="nav-item has-treeview menu-open">
            <a href="../listes/accueil1.php" class="nav-link active">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Accueil
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../listes/accueil1.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard</p>
                </a>
              </li>
            </ul>
          </li>

          <!---- MENU UTILISATEUR------->

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                Utilisateurs
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../listes/listutilisateur1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste des utilisateurs</p>
                </a>
              </li>
            </ul>
          </li>
        
          <!---- MENU CLIENTS------->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Clients
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../listes/ajouclient1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ajouter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../listes/listeclient1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste des clients</p>
                </a>
              </li>
              
            </ul>
          </li>

           <!---- MENU Contact------->
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-phone"></i>
              <p>
                Contact
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../listes/ajoucontact1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ajouter</p>
                </a>
              </li>
             
              <li class="nav-item">
                <a href="../listes/listcontact1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste des contact</p>
                </a>
              </li>
            </ul>
          </li>
 <!---- MENU catigorie d'equipement------->

 <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-list-alt"></i>
              <p>
                Catégories EQP
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="../listes/ajoutcatego1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ajouter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../listes/listeceqp1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste</p>
                </a>
              </li>
            </ul>
          </li>

        <!---- MENU Equipement------->

 <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-dolly "></i>
              <p>
                Eequipement
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../listes/ajouequip1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ajouter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../listes/listequipement1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste</p>
                </a>
              </li>
            </ul>
          </li>

          <!---- MENU catigorie document------->

 <li class="nav-item has-treeview">
            <a href="../listes/" class="nav-link">
              <i class="nav-icon far fa-list-alt"></i>
              <p>
                Catégories DOC
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
             
              <li class="nav-item">
                <a href="../listes/ajoucdoc1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ajouter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../listes/listecdoc1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste</p>
                </a>
              </li>
            </ul>
          </li>

            <!---- MENU  documents------->

 <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                 Documents
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">   
              <li class="nav-item">
                <a href="../listes/ajoudoc1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ajouter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../listes/listedoc1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste</p>
                </a>
              </li>
            </ul>
          </li>
        
  <!---- MENU contrat------->

  <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file "></i>
              <p>
                Contrat
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="../listes/ajoucont1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ajouter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../listes/listecontrat1.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Liste</p>
                </a>
              </li>
            </ul>
          </li>

  <!---- MENU Deconnexion------->

  <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-power-off "></i>
              <p>
                Deconnexion
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../logout.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Se Deconnecter</p>
                </a>
              </li>
              </ul>


          </li>
          <li class="nav-item">
            <a href="pages/gallery.html" class="nav-link">
              
              
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
             
             
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
