
  <div class="container spacer col-md-6 col-xs-12 col-md-offset-3">
			<form class="well" method="post">
				<legend class="alert alert-info" style="text-align: center;">Enregistrement</legend>
		
			<div class="form-group">
					<label for="libelle_equpe">Libelle d'équipement </label>
	 				<input type="text" required="renseigner ce champ" required="Champs Obligatoire" class="form-control" id="libelle_equpe" name="libelle_equpe" placeholder="Libelle d'équipement ">
				</div>
 
			<div class="form-group">
					<label for="numero_serie">Numero de Série </label>
					<input type="text" required="renseigner ce champ" class="form-control" id="numero_serie" name="numero_serie" placeholder="Numéro de série ">
		 		</div>
              <?php
              
                 $b= $db->query('SELECT * FROM type_equipement');
                 $options = "";
                  while ($d = $b->fetch()) 
                 $options = $options."<option value=".$d['id_type'].">".$d['type']."</option>";						
				{ ?>
              <?php } ?>
				
					<div class="form-group">
						<label for="type">Type D'equipement</label>
						<select class=" form-control col-12" id="lib" required="renseigner ce champ" name="id_type">
						<option value="">Aucun</option>
						<?php echo $options; ?>
						</select>
				</div>
				<?php
			    /*
                 $b= $db->query('SELECT * FROM contrat');
                 $options = "";
                 while ($d = $b->fetch())
                 $options = $options."<option value=".$d['idc'].">".$d['libelle']."</option>";						
		
				  { ?>

              <?php }*/ ?>
						<div class="form-group">
						<label for="sous_contrat">Sous contrat</label>

						<select class=" form-control col-12" id="slec" required="renseigner ce champ" name="sous_contrat">
						<option value="">Aucun</option>
						<option value="oui">Oui</option>
					    <option value="nom">Non </option>
						<?php //echo $options; ?>
						</select>
						</div>

					<?php
					
					$b= $db->query('SELECT * FROM client');
                    $options = "";
                    while ($d = $b->fetch())
                 $options = $options."<option value=".$d['idclt'].">".$d['nom_societe']."</option>";						
				 { ?>
              <?php } ?>
					
					<div class="form-group">
						<label for="lib">Client</label>
						<select class=" form-control col-12" id="slec" required="renseigner ce champ" name="idclt">
						<option value="">Aucun</option>
						<?php echo $options; ?>
						</select>
					</div>
				
				<div class="form-group">
					<label for="so">Proprietaire </label>
					<input type="text" required="renseigner ce champ" class="form-control" id="proprietaire" name="proprietaire" placeholder="Proprietaire ">
				</div>
                <div class="form-group">
                	<label for="date">Date</label>
					<input type="date"  required="renseigner ce champ" name="date_en" id="date" class="form-control" placeholder="Date">
				</div>
	           
				<div class="form-group text-center">
					<button class="btn btn-primary" name="valider" id="valider">Valider</button>
					<button class="btn btn-primary" name="annuler" id="annuler">Annuler</button>
				</div>
				
	</form>
  </div>