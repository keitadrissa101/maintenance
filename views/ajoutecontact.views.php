    <div class="container spacer col-md-6 col-xs-12 col-md-offset-3">
			<form class="well" method="post">
				<legend class="alert alert-info" style="text-align: center;">Enregistrement</legend>
				<div class="form-group">
					<label for="nom">Nom :</label>
					<input type="text" name="nom" id="nom" class="form-control">
				</div>
				<div class="form-group">
					<label for="prenom">Prenom :</label>
					<input type="text" name="prenom" id="prenom" class="form-control">
				</div>
				<div class="form-group">
					<label for="poste">Poste :</label>
					<input type="text" name="poste" id="poste" class="form-control">
				</div>
				
	 			<div class="form-group">
					<label for="telephone">Telephone :</label>
					<input type="text" name="telephone" id="telephone" class="form-control">
				</div>
                 <div class="form-group">
					<label for="email">Email :</label>
					<input type="text" name="email" id="email" class="form-control">
				</div>

				<div class="form-group">
					<label for="commentaire">Commentaire :</label>
					<input type="text" name="commentaire" id="commentaire" class="form-control">
				</div>
				<div class="form-group text-center">
					<button class="btn btn-primary" name="valider" id="valider">Valider</button>
					<button class="btn btn-primary" name="annuler" id="annuler">Annuler</button>
				</div>
	</form>
  </div>
