<div class="login-box">
  <div class="login-logo">
    <a href="..listes/index.php"><b>GS</b>MAIN</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Renseignez les champs</p>

      <?php
                     
                if(isset($_SESSION['status']) && $_SESSION['status'] !='')
                {
                    echo '<h4 class="bg-danger text-white">'.$_SESSION['status'].'</h4>';
                }
            ?>

      <form action="system/code.php" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Nom d'utilisateur" name="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Mot de Passe" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Maintenir la connexion
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-8">
            <button type="submit" name="loginbtn" class="btn btn-primary btn-block">Connexion</button>
          </div>
        
      </form>

     
      <!-- /.social-auth-links -->
      <hr>

      <p class="mb-1">
        <a href="forgot_password.php">Mot de passe oublié ?</a>
      </p>
    
    </div>
    <!-- /.login-card-body -->
  </div>
</div>