
			<div class="container spacer col-md-6 col-xs-12 col-md-offset-3">
			<form class="well" method="post">
				<legend class="alert alert-info" style="text-align: center;">Enregistrement</legend>
				<div class="form-group">
					<label for="code_contrat">Code contrat :</label>
					<input type="text" name="code_contrat" id="code_contrat" class="form-control">
				</div>
				<div class="form-group">
					<label for="libelle">Libellé contrat :</label>
					<input type="text" name="libelle" id="libelle" class="form-control">
				</div>
				<div class="form-group">
					<label for="date_debut">Date debut :</label>
 					<input type="date" name="date_debut" id="date_debut" class="form-control">
				</div>
				
				<div class="form-group">
					<label for="date_fin">Date fin :</label>
					<input type="date" name="date_fin" id="date_fin" class="form-control">
				</div>
				<div class="form-group">
					<label for="montant">Montant :</label>
					<input type="text" name="montant" id="montant" class="form-control">
				</div>
				<div class="form-group">
					<label for="type">Type:</label>
					<select id="type_contrat" name="type_contrat" class="form-control">
					    <option value="preventive">Preventive</option>
					    <option value="curative">Curative</option>
					</select>
				</div>
				<div class="form-group">
					<label for="frequence">Frequence:</label>
					<select id="frequence" name="frequence" class="form-control">
					    <option value="1 fois">1fois</option>
					    <option value="2 fois">2fois</option>
					    <option value="3 fois">3fois</option>
					</select>
				</div>
				<div class="form-group text-center">
					<button class="btn btn-primary" name="valider" id="valider">Valider</button>
					<button class="btn btn-primary" name="annuler" id="annuler">Annuler</button>
				</div>
			</form>
			</div>
	