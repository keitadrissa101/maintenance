    <div class="container spacer col-md-6 col-xs-12 col-md-offset-3">
			<form class="well" method="post">
				<legend class="alert alert-info" style="text-align: center;">Enregistrement</legend>
				<div class="form-group">
					<label for="codeclt">Code Client :</label>
					<input type="text" name="codeclt" id="codeclt" class="form-control">
				</div>
				<div class="form-group">
					<label for="nom_societe">Nom Societe :</label>
	 				<input type="text" name="nom_societe" id="nom_societe" class="form-control">
				</div>
				<div class="form-group">
					<label for="adresse">Adresse :</label>
					<input type="text" name="adresse" id="adresse" class="form-control">
				</div>
				
	 			<div class="form-group">
					<label for="telephone">Telephone :</label>
					<input type="text" name="telephone" id="telephone" class="form-control">
				</div>
                 <div class="form-group">
					<label for="email">Email :</label>
					<input type="text" name="email" id="email" class="form-control">
				</div>

				<div class="form-group">
					<label for="datee">Date :</label>
					<input type="date" name="datee" id="datee" class="form-control">
				</div>
				<div class="form-group text-center">
					<button class="btn btn-primary" name="valider" id="valider">Valider</button>
					<button class="btn btn-primary" name="annuler" id="annuler">Annuler</button>
				</div>
	</form>
  </div>
