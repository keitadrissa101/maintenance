 
<style>
    .dataTables_length{
        margin-botton: 30px;
    } 
    .dataTables_length select{
        border: 1px solid #e4e4e4;
    } 
    .dt-buttons a{
        margin-left: 12px; 
        font-size: 12px;
        padding: 6px;
        border: 1px solid #e4e4e4;
        background: #FFF;
        box-shadow: 0px 0px 14px 0px #ececec;

    }
    .dataTabes_filter input{
        border: 1px solid #e4e4e4;
    }
    .table-striped tbody tr{
        line-height: 30px;

    }
    
</style>
    <div class="table-responsive">
            <table id="custom_data" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th><th>LIBELLE</th><th>NUMERO SERIE</th><th>TYPE EQUIPEMENT</th><th>SOUS CONTRAT</th><<th>CLIENT</th><th>PROPRIETAIRE</th> <th>DATE</th><th>EDITION</th><th>SUPRESSION</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $q= $db->query('SELECT * FROM equipement LEFT JOIN client on equipement.idclt=client.idclt LEFT JOIN type_equipement  on equipement.id_type=type_equipement.id_type');
       // $q = $db->query('SELECT * FROM equipement,client,type_equipement WHERE equipement.client=client.idclt AND equipement.typee=type_equipement.id_eq');     
        ?>
         <?php while ($row= $q->fetch()) {?>
                    <tr>
                        <td><?php  echo $row['id_eq']; ?></td>
                        <td><?php  echo $row['libelle_equpe']; ?></td>
                        <td><?php  echo $row['numero_serie']; ?></td>
                        <td><?php  echo $row['id_type']; ?></td>
                        <td><?php  echo $row['sous_contrat']; ?></td>
                        <td><?php  echo $row['idclt']; ?></td>
                        <td><?php  echo $row['proprietaire']; ?></td>
                        <td><?php  echo $row['date_en']; ?></td>
                        <td>
                            <form action="#" method="post">
                                <input type="hidden" name="edit_id" value="<?php  echo $row['id_eq']; ?>">
                                <button class="btn btn-success far fa-edit" name="editeqp" type="submit"></button>
                            </form>
                        </td>    
                        <td>
                            <form action="#" method="post">
                                <input type="hidden" name="delete_id" value="<?php  echo $row['id_equipement']; ?>">
                                <button class="btn btn-danger far fa-trash-alt" name="deletebtnequipement" type="submit"></button>
                            </form>              
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
   </div>
 </div>
 </div>